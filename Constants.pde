final int ROWS = 10;
final int COLS = 14;
final int CELL_SIZE = 45;

final int X = 0, I = 1, R = 2, B = 3, G = 4, C = 5;

int[][] puzzle = {
	{X, X, X, X, X, X, X, X, X, X, X, X, X, X},
	{X, I, I, I, I, I, I, I, I, I, I, I, I, X},
	{X, I, I, I, I, I, I, I, I, I, I, I, I, X},
	{X, I, I, I, I, I, I, I, I, I, I, I, I, X},
	{X, I, I, I, I, I, I, I, I, I, I, I, I, X},
	{X, I, I, I, I, I, I, I, R, I, I, I, I, X},
	{X, I, I, I, I, I, I, X, X, I, I, I, I, X},
	{X, I, I, G, I, I, I, I, I, R, I, B, I, X},
	{X, X, B, X, X, X, G, I, X, X, X, X, X, X},
	{X, X, X, X, X, X, X, X, X, X, X, X, X, X} };
