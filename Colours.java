enum Colour {
	BROWN,			// terrain
	GREY,			// terrain
	PURPLE,			// terrain
	RED, 			// jelly
	BLUE, 			// jelly
	GREEN,			// jelly
	YELLOW,			// jelly
	BLACK			// helper jelly
};
