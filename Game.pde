void setup() {
	size(COLS * CELL_SIZE, ROWS * CELL_SIZE);
	background(255);
}

void draw() {
	for (int i = 0; i < COLS; i++) {
		for (int j = 0; j < ROWS; j++) {
			if (puzzle[j][i] == X) {
				fill(205, 133, 63);
			} else if (puzzle[j][i] == R) {
				fill(255, 69, 0);
			} else if (puzzle[j][i] == B) {
				fill(123, 104, 238);
			} else if (puzzle[j][i] == G) {
				fill(33, 232, 57);
			}  else if (puzzle[j][i] == I) {
				fill(255);
			}
			noStroke();
			rect(CELL_SIZE * i, CELL_SIZE * j, CELL_SIZE - 1, CELL_SIZE - 1);
		}
	}
}

void mousePressed() {
	int x = (int) mouseX/CELL_SIZE;
	int y = (int) mouseY/CELL_SIZE;

	move(x, y, mouseButton); // mouseButton = {LEFT, RIGHT}

	gravity();
}

void move(int x, int y, int dir) {
	if (dir == LEFT) {
		if (x > 0 && puzzle[y][x - 1] == I) {
			puzzle[y][x - 1] = puzzle[y][x];
			puzzle[y][x] = I;
		}
	} else {
		if (x < COLS && puzzle[y][x + 1] == I) {
			puzzle[y][x + 1] = puzzle[y][x];
			puzzle[y][x] = I;
		}
	}
}

void gravity() {
	int x, y;
	for (x = 0; x < COLS; x++) {
		for (y = 0; y < ROWS - 1; y++) {
			if (puzzle[y][x] == R || puzzle[y][x] == G || puzzle[y][x] == B) {
				int y2 = y;
				while (puzzle[y2 + 1][x] == I) {
					y2++;
				}
				if (y2 != y) {
					puzzle[y2][x] = puzzle[y][x];
					puzzle[y][x] = I;
				}
			}
		}
	}
}
